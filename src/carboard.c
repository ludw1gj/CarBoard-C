#include "carboard.h"

int main() {
   char input[NUMBERED_INPUT_LENGTH];
   int running = TRUE;

   while (running) {
      printf("Welcome to Car Board\n");
      printf("--------------------\n");
      printf("1. Play game\n");
      printf("2. Quit\n\n");

      printf("Please enter your input: ");

      getInput(input);
      if (strcmp(input, "1") == EQUAL) {
         playGame();
      } else if (strcmp(input, "2") == EQUAL) {
         running = FALSE;
         printf("\nGood bye!\n");
      } else {
         printf("\nInvalid Input.\n\n");
      }
   }
   return EXIT_SUCCESS;
}
