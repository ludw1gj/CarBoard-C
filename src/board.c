#include "board.h"

Cell BOARD_1[BOARD_HEIGHT][BOARD_WIDTH] = {
   {BLOCKED, EMPTY,   EMPTY,   EMPTY, EMPTY,   EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY},
   {EMPTY,   BLOCKED, EMPTY,   EMPTY, EMPTY,   EMPTY,   EMPTY,   BLOCKED, EMPTY, EMPTY},
   {EMPTY,   EMPTY,   BLOCKED, EMPTY, EMPTY,   EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY},
   {EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY,   EMPTY,   EMPTY,   BLOCKED, EMPTY, EMPTY},
   {EMPTY,   EMPTY,   EMPTY,   EMPTY, BLOCKED, EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY},
   {EMPTY,   EMPTY,   BLOCKED, EMPTY, EMPTY,   BLOCKED, EMPTY,   BLOCKED, EMPTY, EMPTY},
   {EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY,   EMPTY,   BLOCKED, EMPTY,   EMPTY, EMPTY},
   {EMPTY,   EMPTY,   BLOCKED, EMPTY, EMPTY,   EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY},
   {EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY,   EMPTY,   EMPTY,   EMPTY,   EMPTY, EMPTY},
   {EMPTY,   EMPTY,   BLOCKED, EMPTY, EMPTY,   EMPTY,   EMPTY,   EMPTY,   EMPTY, BLOCKED}
};

Cell BOARD_2[BOARD_HEIGHT][BOARD_WIDTH] = {
   {BLOCKED, BLOCKED, BLOCKED, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
   {EMPTY,   BLOCKED, BLOCKED, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
   {EMPTY,   BLOCKED, BLOCKED, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
   {EMPTY,   EMPTY,   EMPTY,   EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
   {EMPTY,   EMPTY,   EMPTY,   EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
   {EMPTY,   BLOCKED, BLOCKED, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
   {EMPTY,   EMPTY,   EMPTY,   EMPTY, BLOCKED, EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
   {EMPTY,   BLOCKED, BLOCKED, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
   {EMPTY,   BLOCKED, BLOCKED, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY, EMPTY, EMPTY},
   {EMPTY,   BLOCKED, BLOCKED, EMPTY, EMPTY,   EMPTY, EMPTY, EMPTY, EMPTY, EMPTY}
};

void initialiseBoard(Cell board[BOARD_HEIGHT][BOARD_WIDTH]) {
   int row;
   int column;

   for (row = 0; row < BOARD_HEIGHT; row++) {
      for (column = 0; column < BOARD_WIDTH; column++) {
         board[row][column] = EMPTY;
      }
   }
}

void loadBoard(Cell board[BOARD_HEIGHT][BOARD_WIDTH],
               Cell boardToLoad[BOARD_HEIGHT][BOARD_WIDTH]) {
   int row;
   int column;

   for (row = 0; row < BOARD_HEIGHT; row++) {
      for (column = 0; column < BOARD_WIDTH; column++) {
         board[row][column] = boardToLoad[row][column];
      }
   }
}

Boolean placePlayer(Cell board[BOARD_HEIGHT][BOARD_WIDTH], Position position) {
   if (!isWithinBounds(position)) {
      return FALSE;
   }
   if (board[position.y][position.x] == BLOCKED) {
      return FALSE;
   }
   if (board[position.y][position.x] == EMPTY) {
      board[position.y][position.x] = PLAYER;
      return TRUE;
   }
   return FALSE;
}

PlayerMove movePlayerForward(Cell board[BOARD_HEIGHT][BOARD_WIDTH],
                             Player *player) {
   Position newPosition = getNextForwardPosition(player);
   if (!isWithinBounds(newPosition)) {
      return OUTSIDE_BOUNDS;
   }
   switch (board[newPosition.y][newPosition.x]) {
      case EMPTY:
         /* remove player from previous position on the board and set new
          * position for player on the board */
         board[player->position.y][player->position.x] = EMPTY;
         placePlayer(board, newPosition);

         /* update the player's position */
         updatePosition(player, newPosition);
         return PLAYER_MOVED;
      default:
         return CELL_BLOCKED;
   }
}

void displayBoard(Cell board[BOARD_HEIGHT][BOARD_WIDTH], Player *player) {
   int row;
   int column;

   printf("\n| |0|1|2|3|4|5|6|7|8|9|\n");
   for (row = 0; row < BOARD_HEIGHT; row++) {
      printf("|%d|", row);
      for (column = 0; column < BOARD_WIDTH; column++) {
         switch (board[row][column]) {
            case EMPTY:
               printf(EMPTY_OUTPUT);
               break;
            case BLOCKED:
               printf(BLOCKED_OUTPUT);
               break;
            case PLAYER:
               if (!player) {
                  break;
               }
               switch (player->direction) {
                  case NORTH:
                     displayDirection(NORTH);
                     break;
                  case EAST:
                     displayDirection(EAST);
                     break;
                  case SOUTH:
                     displayDirection(SOUTH);
                     break;
                  case WEST:
                     displayDirection(WEST);
               }
         }
         printf("|");
      }
      printf("\n");
   }
   printf("\n");
}

Boolean isWithinBounds(Position position) {
   int invalidY = position.y < 0 || position.y >= BOARD_HEIGHT;
   int invalidX = position.x < 0 || position.x >= BOARD_WIDTH;
   if (invalidY || invalidX) {
      return FALSE;
   }
   return TRUE;
}
