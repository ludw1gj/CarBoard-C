#include "game.h"

void playGame() {
   char input[INPUT_LENGTH];
   Cell board[BOARD_HEIGHT][BOARD_WIDTH];
   Player player;
   int running = TRUE;

   /* initialise board and display info */
   initialiseBoard(board);
   printf("\nYou can use the following commands to play the game: load <g>\n");
   printf("\tg: number of the game board to load\n");
   printf("init <x>,<y>,<direction>\n");
   printf("\tx: horizontal position of the car on the board (between 0 & 9)\n");
   printf("\ty: vertical position of the car on the board (between 0 & 9)\n");
   printf("\tdirection: direction of the car’s movement (north, east, south, "
          "west)\n");
   printf("forward (or f)\n");
   printf("turn_left (or l)\n");
   printf("turn_right (or r)\n");
   printf("quit\n\n");

   printf("Press enter to continue...");
   getInput(input);

   /* Game loops:
    * Display the board, display commands, listen for user input, compare user
    * input to commands, if user input was
    * invalid - display "Invalid Input.", and if user input was valid - process
    * command.
    *
    * Exit loop when running variable is no longer true (which the quit command
    * changes it to false) and break the loop
    * when appropriate to go the next stage of the program / next loop (eg.
    * load command in first loop break the loop
    * to go to the loop/stage)
    * */
   while (running) {
      displayBoard(board, NULL);
      printf("At this stage of the program, only two commands are acceptable:\n");
      printf("load <g>\n");
      printf("quit\n\n");

      getInput(input);
      if (handleLoadCommand(input, board)) {
         /* if load command processed successfully, go to next stage */
         break;
      }
      if (!handleQuitCommand(input, &running)) {
         printf("\nInvalid Input.\n");
      }
   }

   while (running) {
      displayBoard(board, &player);
      printf("At this stage of the program, only three commands are acceptable:\n");
      printf("load <g>\n");
      printf("init <x>,<y>,<direction>\n");
      printf("quit\n\n");

      getInput(input);
      if (handleInitCommand(input, &player)) {
         if (placePlayer(board, player.position)) {
            /* if valid position, player is placed on board in position (x,y)
             * and to go next stage */
            break;
         }
         printf("\nUnable to place player at that position.\n");
      } else if (!handleLoadCommand(input, board) &&
                 !handleQuitCommand(input, &running)) {
         printf("\nInvalid Input.\n");
      }
   }

   while (running) {
      displayBoard(board, &player);
      printf("At this stage of the program, only four commands are acceptable:\n");
      printf("forward (or f)\n");
      printf("turn_left (or l)\n");
      printf("turn_right (or r)\n");
      printf("quit\n\n");

      getInput(input);
      if (!handleForwardCommand(input, board, &player) &&
          !handleTurnLeftCommand(input, &player) &&
          !handleTurnRightCommand(input, &player) &&
          !handleQuitCommand(input, &running)) {
         printf("\nInvalid Input.\n");
      }
      if (!running) {
         printf("\nTotal player moves: %d\n\n", player.moves);
      }
   }
}

Boolean handleQuitCommand(char input[INPUT_LENGTH], int *running) {
   /* command: quit */
   if (strcmp(input, COMMAND_QUIT) != EQUAL) {
      return FALSE;
   };
   *running = FALSE;
   return TRUE;
}

Boolean handleLoadCommand(char input[INPUT_LENGTH],
                          Cell board[BOARD_HEIGHT][BOARD_WIDTH]) {
   /* command: load <g> */
   char givenInput[INPUT_LENGTH];
   char *load;
   char *number;
   char *trailing;

   strncpy(givenInput, input, INPUT_LENGTH);

   load = strtok(givenInput, SPACE_SEPARATOR);
   number = strtok(NULL, SPACE_SEPARATOR);
   trailing = strtok(NULL, SPACE_SEPARATOR);

   if (!load || !number || trailing) {
      return FALSE;
   }
   if (strcmp(load, COMMAND_LOAD) != EQUAL) {
      return FALSE;
   }
   if (strcmp(number, "1") == EQUAL) {
      loadBoard(board, BOARD_1);
      return TRUE;
   }
   if (strcmp(number, "2") == EQUAL) {
      loadBoard(board, BOARD_2);
      return TRUE;
   }
   return FALSE;
}

Boolean handleInitCommand(char input[INPUT_LENGTH], Player *player) {
   /* command: init <x>,<y>,<direction> */
   char givenInput[INPUT_LENGTH];
   char *init;
   int x;
   int y;
   char *direction;
   char *trailing;

   Direction playerDirection;
   Position playerPosition;

   strncpy(givenInput, input, INPUT_LENGTH);

   init = strtok(givenInput, SPACE_SEPARATOR);
   x = convertToInt(strtok(NULL, COMMA_SEPARATOR));
   y = convertToInt(strtok(NULL, COMMA_SEPARATOR));
   direction = strtok(NULL, COMMA_SEPARATOR);
   trailing = strtok(NULL, COMMA_SEPARATOR);

   if (!init || !direction || trailing) {
      return FALSE;
   }
   if (strcmp(init, COMMAND_INIT) != EQUAL || x == INT_CONVERSION_ERROR ||
       y == INT_CONVERSION_ERROR) {
      return FALSE;
   }

   /* find direction, initialise player */
   playerPosition.x = x;
   playerPosition.y = y;

   if (strcmp(direction, DIRECTION_NORTH) == EQUAL) {
      playerDirection = NORTH;
   } else if (strcmp(direction, DIRECTION_EAST) == EQUAL) {
      playerDirection = EAST;
   } else if (strcmp(direction, DIRECTION_SOUTH) == EQUAL) {
      playerDirection = SOUTH;
   } else if (strcmp(direction, DIRECTION_WEST) == EQUAL) {
      playerDirection = WEST;
   } else {
      return FALSE;
   }
   initialisePlayer(player, &playerPosition, playerDirection);
   return TRUE;
}

Boolean handleForwardCommand(char input[INPUT_LENGTH],
                             Cell board[BOARD_HEIGHT][BOARD_WIDTH],
                             Player *player) {
   /* command: forward (or f) */
   PlayerMove playerMoved;

   if (strcmp(input, COMMAND_FORWARD) != EQUAL &&
       strcmp(input, COMMAND_FORWARD_SHORTCUT) != EQUAL) {
      return FALSE;
   }
   playerMoved = movePlayerForward(board, player);
   switch (playerMoved) {
      case PLAYER_MOVED:
         printf("\nPlayer moved.\n");
         break;
      case CELL_BLOCKED:
         printf("\nCannot move forward because the road is blocked.\n");
         break;
      case OUTSIDE_BOUNDS:
         printf("\nThe car is at the edge of the board and cannot move further "
                "in that direction.\n");
   }
   return TRUE;
}

Boolean handleTurnLeftCommand(char input[INPUT_LENGTH], Player *player) {
   /* command: turn_left (or l) */
   if (strcmp(input, COMMAND_TURN_LEFT) != EQUAL &&
       strcmp(input, COMMAND_TURN_LEFT_SHORTCUT) != EQUAL) {
      return FALSE;
   }
   turnDirection(player, TURN_LEFT);
   return TRUE;
}

Boolean handleTurnRightCommand(char input[INPUT_LENGTH], Player *player) {
   /* command: turn_right (or r) */
   if (strcmp(input, COMMAND_TURN_RIGHT) != EQUAL &&
       strcmp(input, COMMAND_TURN_RIGHT_SHORTCUT) != EQUAL) {
      return FALSE;
   }
   turnDirection(player, TURN_RIGHT);
   return TRUE;
}
