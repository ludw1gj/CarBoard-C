#include "helpers.h"

void readRestOfLine() {
   int ch;
   while (ch = getc(stdin), ch != EOF && ch != '\n') {
   } /* Gobble each character. */

   /* Reset the error status of the stream. */
   clearerr(stdin);
}

void getInput(char input[INPUT_LENGTH]) {
   fgets(input, INPUT_LENGTH, stdin);

   /* if no trailing newline then buffer overflow */
   if (!strchr(input, '\n')) {
      readRestOfLine();
      return;
   }
   /* remove trailing newline */
   input[strlen(input) - 1] = '\0';
}

int convertToInt(char *strNumber) {
   char *end;
   int result;

   if (strNumber == NULL) {
      return -1;
   }
   result = (int) strtol(strNumber, &end, 10);

   if (!*end) {
      return result;
   }
   /* error in conversion */
   return -1;
}
