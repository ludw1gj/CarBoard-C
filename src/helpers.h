#ifndef HELPERS_H
#define HELPERS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

typedef enum boolean {
   FALSE = 0,
   TRUE
} Boolean;

#define NEW_LINE_SPACE 1
#define NULL_SPACE 1

/**
 * This is used to compensate for the extra character spaces taken up by
 * the '\n' and '\0' when input is read through fgets().
 **/
#define EXTRA_SPACES (NEW_LINE_SPACE + NULL_SPACE)

#define EMPTY_STRING ""

/**
 * My extra stuff
 **/
#define INPUT_LENGTH 20
#define NUMBERED_INPUT_LENGTH 2
#define SPACE_SEPARATOR " "
#define COMMA_SEPARATOR ","
#define INT_CONVERSION_ERROR (-1)
#define EQUAL 0

/**
 * Call this function whenever you detect buffer overflow but only call this
 * function when this has happened.
 **/
void readRestOfLine();

/**
 * Gets input from stdin and detects buffer overflow. Also, it will remove
 * trailing newline at the end of the input.
 **/
void getInput(char input[INPUT_LENGTH]);

/**
 * Converts a char array representing a number to type int. Returns -1 if
 * conversion failed.
 **/
int convertToInt(char *input);

#endif
