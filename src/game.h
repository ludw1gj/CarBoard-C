#ifndef GAME_H
#define GAME_H

#include "helpers.h"
#include "board.h"
#include "player.h"

#define COMMAND_LOAD "load"
#define COMMAND_INIT "init"
#define COMMAND_FORWARD "forward"
#define COMMAND_FORWARD_SHORTCUT "f"
#define COMMAND_TURN_LEFT "turn_left"
#define COMMAND_TURN_LEFT_SHORTCUT "l"
#define COMMAND_TURN_RIGHT "turn_right"
#define COMMAND_TURN_RIGHT_SHORTCUT "r"
#define COMMAND_QUIT "quit"

#define DIRECTION_NORTH "north"
#define DIRECTION_EAST "east"
#define DIRECTION_SOUTH "south"
#define DIRECTION_WEST "west"

/**
 * Main menu option 1 - play the game as per the specification.
 *
 * This function makes all the calls to board & player and handles interaction
 * with the user (reading input from the console, error checking, etc...).
 *
 * It should be possible to break this function down into smaller functions -
 * but this is up to you to decide and is entirely your own design. You can
 * place the additional functions in this header file if you want.
 *
 * Note that if you don't break this function up it could become pretty big...
 */
void playGame();

/**
 * Handle Quit Command.
 *
 * Accepts user input and a pointer to an int as parameters. It compares the
 * user input to "quit", and will it change the int pointer to 0 (FALSE) and
 * return TRUE if the input was "quit".
 */
Boolean handleQuitCommand(char input[INPUT_LENGTH], int *running);

/**
 * Handle Load Command.
 *
 * Accepts user input and the board as parameters. It compares the user input
 * and will load either board 1 or board 2.
 */
Boolean handleLoadCommand(char input[INPUT_LENGTH],
                               Cell board[BOARD_HEIGHT][BOARD_WIDTH]);

/**
 * Handle Init Command.
 *
 * Accepts user input, and the player as parameters. It compares the user input
 * and will initialise the player.
 */
Boolean handleInitCommand(char input[INPUT_LENGTH], Player *player);

/**
 * Handle Forward Command.
 *
 * Accepts user input, board, and the player as parameters. It compares the user
 * input and will move player forward if it is a valid move.
 */
Boolean handleForwardCommand(char input[INPUT_LENGTH],
                                  Cell board[BOARD_HEIGHT][BOARD_WIDTH],
                                  Player *player);

/**
 * Handle Turn Left Command.
 *
 * Accepts user input, and the player as parameters. It compares the user input
 * and will turn the player to the left, eg. North -> West.
 */
Boolean handleTurnLeftCommand(char input[INPUT_LENGTH], Player *player);

/**
 * Handle Turn Right Command.
 *
 * Accepts user input, and the player as parameters. It compares the user input
 * and will turn the player to the right, eg. North -> East.
 */
Boolean handleTurnRightCommand(char input[INPUT_LENGTH], Player *player);

#endif
