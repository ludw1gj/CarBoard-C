#include "player.h"

void initialisePlayer(Player *player, Position *position, Direction direction) {
   player->position = *position;
   player->direction = direction;
   player->moves = 0;
}

void turnDirection(Player *player, TurnDirection turnDirection) {
   if (turnDirection == TURN_RIGHT) {
      switch (player->direction) {
         case NORTH:
            player->direction = EAST;
            break;
         case EAST:
            player->direction = SOUTH;
            break;
         case SOUTH:
            player->direction = WEST;
            break;
         case WEST:
            player->direction = NORTH;
      }
   }
   if (turnDirection == TURN_LEFT) {
      switch (player->direction) {
         case NORTH:
            player->direction = WEST;
            break;
         case WEST:
            player->direction = SOUTH;
            break;
         case SOUTH:
            player->direction = EAST;
            break;
         case EAST:
            player->direction = NORTH;
      }
   }
}

Position getNextForwardPosition(const Player *player) {
   Position position = player->position;

   switch (player->direction) {
      case NORTH:
         position.y--;
         break;
      case EAST:
         position.x++;
         break;
      case SOUTH:
         position.y++;
         break;
      case WEST:
         position.x--;
   }
   return position;
}

void updatePosition(Player *player, Position position) {
   player->position = position;
   player->moves++;
}

void displayDirection(Direction direction) {
   switch (direction) {
      case NORTH:
         printf(DIRECTION_ARROW_OUTPUT_NORTH);
         break;
      case EAST:
         printf(DIRECTION_ARROW_OUTPUT_EAST);
         break;
      case SOUTH:
         printf(DIRECTION_ARROW_OUTPUT_SOUTH);
         break;
      case WEST:
         printf(DIRECTION_ARROW_OUTPUT_WEST);
   }
}
